import { useEffect, useState } from "react";
import styled from "styled-components";

const Qted = styled.div`
  min-height: 100vh;
  background-image: url("../src/bg.jpg");
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
  .card {
    background: rgba(255, 255, 255, 0.25);
    box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
    backdrop-filter: blur(13.5px);
    -webkit-backdrop-filter: blur(13.5px);
    border-radius: 10px;
    height: 80px;
    padding: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  p {
    color: white;
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 18px;
    margin: 0;
    padding: 0;
  }
`;

const homePage = () => {
  const [quote, setQuote] = useState(null);
  useEffect(() => {
    document.title = 'Fun Jokes'
    fetch(
      "https://webknox-jokes.p.rapidapi.com/jokes/random?maxLength=100&minRating=8",
      {
        method: "GET",
        headers: {
          "x-rapidapi-key":
            "4c6d1c7dcdmsh6b1819714e3b1c4p1ba982jsn5b62cc56dbd7",
          "x-rapidapi-host": "webknox-jokes.p.rapidapi.com",
        },
      }
    )
      .then((ress) => ress.json())
      .then((res) => setQuote(res.joke));
  }, [Qted]);
  return (
    <Qted>
      <div className="card">
        <p>{quote}</p>
      </div>
    </Qted>
  );
};

export default homePage;
